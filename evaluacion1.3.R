# 3) A doce pares de gemelos se les realizó un test psicológico para medir (en un score) la agresividad en cada
# uno. Estamos interesados en comparar los gemelos uno con otro para ver si el gemelo que nació primero
# tiende a ser más agresivo que el otro, donde un score más alto indica mayor agresividad. Datos “carácter
# de los gemelos”.
gemelos <- read_excel("Datos_Eval.xlsx", sheet = "caracter de los gemelos")

# Test con homogeneidad y varianza
# Las escuelas tienen el mismo puntaje
wilcox.test(gemelos$ScoreG1, gemelos$ScoreG2, alternative='two.sided', exact=TRUE, paired=TRUE)
# Acepta con pvalor < .3428

